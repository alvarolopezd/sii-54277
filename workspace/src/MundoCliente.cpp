// MundoCliente.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
//#include "Plano.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <signal.h>

#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/mman.h>
#include "Puntuaciones.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	munmap(pdatosCompartidos,sizeof(datosCompartidos));
	unlink("/tmp/datosCompartidos");
	//close (fd);
	
	//close(fifo_servidor_cliente);
	//unlink("FIFO_SERVIDOR_CLIENTE");

	
	//close(fifo_cliente_servidor);
	//unlink("FIFO_CLIENTE_SERVIDOR");
	
	
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	//esfera2.Dibuja();

	
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	/*Puntuaciones puntos;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	//esfera2.Mueve(0.025f);
		
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		//paredes[i].Rebota(esfera2);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	//jugador1.Rebota(esfera2);
	//jugador2.Rebota(esfera2);
	
	//Actualizo los datos de la memoria compartida
	pdatosCompartidos->esfera=esfera;
	pdatosCompartidos->raqueta1=jugador1;

	if(pdatosCompartidos->accion==-1){OnKeyboardDown('s', 0, 0);}

	else if(pdatosCompartidos->accion==0){}

	else if(pdatosCompartidos->accion==1){OnKeyboardDown('w', 0, 0);}
	
	/////
	
	//if(fondo_izq.Rebota(esfera)||fondo_izq.Rebota(esfera2))
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		
		//esfera2.centro.x=0;
		//esfera2.centro.y=rand()/(float)RAND_MAX;
		//esfera2.velocidad.x=2+2*rand()/(float)RAND_MAX;
		//esfera2.velocidad.y=2+2*rand()/(float)RAND_MAX;
		
		puntos2++;
		//logger
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWinner=2;
		write(fd,&puntos,sizeof(puntos));
		
	}


	//if(fondo_dcho.Rebota(esfera)||fondo_dcho.Rebota(esfera2))
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		
		//esfera2.centro.x=0;
		//esfera2.centro.y=rand()/(float)RAND_MAX;
		//esfera2.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		//esfera2.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		
		puntos1++;
		//logger
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWinner=1;
		write(fd,&puntos,sizeof(puntos));
		

	}
	
	//if(jugador1.Rebota(esfera)==true||jugador1.Rebota(esfera2)==true)
	if(jugador1.Rebota(esfera)==true)
	{
		jugador1.y1+=0.1f;
		jugador1.y2-=0.1f;
	}
	//if(jugador2.Rebota(esfera)==true||jugador1.Rebota(esfera2)==true)
	if(jugador2.Rebota(esfera)==true)
	{
		jugador2.y1+=0.1f;
		jugador2.y2-=0.1f;
	}
	
	if((puntos1==3)||(puntos2==3))
	{
		this->CMundoCliente::~CMundoCliente();
	}*/
	
	//LEEMOS LOS DATOS ENVIADOS POR EL SERVIDOR

	char cad[200];

	//read(fifo_servidor_cliente,cad,sizeof(cad));
	
	Comunicacion_Socket.Receive(cad,sizeof(cad));

	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1, &puntos2); 

	//Actualizo los datos de la memoria compartida
	pdatosCompartidos->esfera=esfera;
	pdatosCompartidos->raqueta1=jugador1;
	
	if(pdatosCompartidos->accion==-1){OnKeyboardDown('s', 0, 0);}
	else if(pdatosCompartidos->accion==0){}
	else if(pdatosCompartidos->accion==1){OnKeyboardDown('w', 0, 0);}

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
	switch(key)
	{
	// case 'a':jugador1.velocidad.x=-1;break;
	// case 'd':jugador1.velocidad.x=1;break;
	//case 's':jugador1.velocidad.y=-4;break;
	//case 'w':jugador1.velocidad.y=4;break;
	
	//case 'l':jugador2.velocidad.y=-4;break;
	//case 'o':jugador2.velocidad.y=4;break;
	
	case 's':sprintf(tecla,"s");break;
        case 'w':sprintf(tecla,"w");break;
        case 'l':sprintf(tecla,"l");break;
        case 'o':sprintf(tecla,"o");break;
        
	}
	//write(fifo_cliente_servidor,&key,sizeof(key));
	Comunicacion_Socket.Send(tecla,sizeof(tecla));
}

void CMundoCliente::Init()
{
	// Iniclizacion de la FIFO
	//if((fd=open("FIFO_LOGGER",O_WRONLY))<0){
	//	perror("No pudo abrirse el FIFO");
	//	return;
	//}
	
	char ip[]="127.0.0.1";
      	char nombre[100];   
     	printf("Introduzca nombre de jugador porfavor:\n");
     	sscanf("%s",nombre);
     	Comunicacion_Socket.Connect(ip,8000);
     	Comunicacion_Socket.Send(nombre,sizeof(nombre));
		
	
	
	
	// Creacion del fichero de memcompartida y mmap
	int fd_mmap;
	//char *pfd;

	fd_mmap= open("/tmp/datosCompartidos", O_CREAT|O_TRUNC|O_RDWR, 0666);

	if (fd_mmap < 0) {
           perror("Error creación fichero destino");
           exit(1);
        }

	datosCompartidos.esfera=esfera;
	datosCompartidos.raqueta1=jugador1;
	datosCompartidos.accion=0;

	write(fd_mmap,&datosCompartidos,sizeof(datosCompartidos));

	pdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosCompartidos), PROT_READ|	PROT_WRITE, MAP_SHARED, fd_mmap, 0));

	close(fd_mmap);
	if (pdatosCompartidos==MAP_FAILED){
		perror("Error en la proyeccion del fichero origen");
		//close(fd_mmap);
		return;

	}
	
	
	////
	
//CREACION Y APERTURA EN MODO LECTURA FIFO SERVIDOR CLIENTE

	/*mkfifo("FIFO_SERVIDOR_CLIENTE",0600);
	fifo_servidor_cliente=open("FIFO_SERVIDOR_CLIENTE",O_RDONLY);
	if(fifo_servidor_cliente<0){
		perror("Error en la creacion de FIFO_SERVIDOR_CLIENTE");
		return;
	}*/
	
	//CREACION Y APERTURA EN MODO LECTURA FIFO CLIENTE SERVIDOR

	/*mkfifo("FIFO_CLIENTE_SERVIDOR",0600);
	fifo_cliente_servidor=open("FIFO_CLIENTE_SERVIDOR",O_WRONLY);
	
	if(fifo_cliente_servidor<0){
		perror("Error en la creacion de FIFO_CLIENTE_SERVIDOR");
		return;
	}*/	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
