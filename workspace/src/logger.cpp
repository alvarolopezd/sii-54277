#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include "Puntuaciones.h"

int main()
{
	int fd;
	Puntuaciones puntos;
	int error;
	
	// Crear FIFO
	error=mkfifo("/tmp/FIFO", 0600);
	if (error<0) {
		perror("No puede crearse el FIFO");
		return 1;
	}
	
	// Abro el FIFO
	fd=open("/tmp/FIFO", O_RDONLY);
	if (fd<0) {
		perror("No puede abrirse el FIFO");
		return 1;
	}

	while (read(fd, &puntos, sizeof(puntos))==sizeof(puntos))  {
		if(puntos.lastWinner==1){
		printf("Jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos.jugador1);
		}
		else if(puntos.lastWinner==2){
		printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
	}
	
	if(read(fd, &puntos, sizeof(puntos))=='\0') // == 0
		return 1;
		
	if(read(fd, &puntos, sizeof(puntos))!=sizeof(puntos))
		return 1;
	
	error=close(fd);
	if(error<0)
	{
		perror("No puede cerrarse el descriptor de fichero");
		return 1;
	}

	error=unlink("/tmp/FIFO");
	if(error<0)
	{
		perror("No puede borrarse la FIFO");
		return 1;
	}

	return 0;

}

