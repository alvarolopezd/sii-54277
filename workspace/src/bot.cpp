#include"DatosMemCompartida.h"
#include "Vector2D.h"
#include "Plano.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>


int main(){
	int fd;
	DatosMemCompartida *pdatosCompartidos;
	fd=open("/tmp/datosCompartidos",O_RDWR);
	if (fd < 0) {
           perror("Error en la apertura del fichero");
           exit(1);
        }

	pdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	if (pdatosCompartidos==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd);
		return(1);
	}
	close(fd);
	
	while(pdatosCompartidos!=NULL){
		float centroraqueta=(pdatosCompartidos->raqueta1.y1+pdatosCompartidos->raqueta1.y2)/2;
		if(pdatosCompartidos->esfera.centro.y<centroraqueta){
			pdatosCompartidos->accion=-1;
		}
		else if(pdatosCompartidos->esfera.centro.y==centroraqueta){
			pdatosCompartidos->accion=0;
		}
		else if(pdatosCompartidos->esfera.centro.y>centroraqueta){
			pdatosCompartidos->accion=1;
		}	
	usleep(25000);
	
	}
	unlink("/tmp/datosCompartidos");
	return 1;
}
