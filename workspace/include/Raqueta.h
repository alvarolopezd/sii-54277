// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	Vector2D centro;

	Raqueta();
	virtual ~Raqueta();
	//Vector2D getCentro();
	
	void Mueve(float t);
};
