// MundoCliente.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
//#include "Puntuaciones.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoCliente  
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	//int fd; // descriptor de fichero FIFO 
	//int fifo_servidor_cliente;
	//int fifo_cliente_servidor;
	//Puntuaciones puntos;
	Socket Comunicacion_Socket;
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	
	Esfera esfera;
	//Esfera esfera2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	DatosMemCompartida datosCompartidos;
	DatosMemCompartida *pdatosCompartidos;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
